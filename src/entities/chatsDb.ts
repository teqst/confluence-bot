import { fileURLToPath } from 'url'
import { dirname, join } from 'path'
import { dbQuery } from './database/index.js'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

export const CHATS_DIRECTORY_NAME = 'chatsData'
export const CHATS_FILE_NAME = 'chatIds.txt'
export const DIR_PATH = join(__dirname, CHATS_DIRECTORY_NAME)

async function initChatsTable() {
  await dbQuery('CREATE TABLE IF NOT EXISTS chats (id SERIAL PRIMARY KEY, chatId INT NOT NULL)')
}

export async function saveChatId(chatId: number) {
  await initChatsTable()
  await dbQuery(`INSERT INTO chats (chatId) VALUES (${chatId})`)
}

export async function getChatIds() {
  const response = await dbQuery('SELECT * FROM chats')
  const chatIds: Array<number> = response.map((chatid: any) => chatid.chatid)
  return Array.from(new Set(chatIds))
}

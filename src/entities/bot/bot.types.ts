export enum TelegramBotCommands {
  START = '/start',
  VERSION = '/version',
  DAILY_JOKE = '/dailyJoke',
}

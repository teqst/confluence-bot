import TelegramBot from 'node-telegram-bot-api'
import {
  MergeRequestEventObject,
  NoteEventObject,
  PipelineEventObject,
  PushEventObject,
} from '../../routes/gitHook/gitHook.types.js'
import {onMergeRequestEvent} from "./handlers/merge.js";
import {onPipelineEvent} from "./handlers/pipeline.js";

export default class TGBot {
  private static instance: TelegramBot

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  private constructor() {}

  static getInstance() {
    if (!TGBot.instance) {
      TGBot.instance = new TelegramBot(String(process.env.TOKEN), { polling: true })
      TGBot.instance.setMyCommands([
        { command: '/version', description: 'Узнать версию бота'},
        { command: '/start', description: 'Подписаться на уведомления'}
        // { command: '/dailyJoke', description: 'Быстрый анекдот для рассказа на дейли'}
      ])
    }

    return TGBot.instance
  }

  static sendMergeRequestMessage(chatId: number, eventBody: MergeRequestEventObject) {
    TGBot.getInstance().sendMessage(
      chatId,
        onMergeRequestEvent(eventBody),
      {
        parse_mode: 'Markdown',
      },
    )
  }

  static sendPipelineMessage(chatId: number, eventBody: PipelineEventObject) {
    if (!onPipelineEvent(eventBody).length) return

    TGBot.getInstance().sendMessage(
      chatId,
      onPipelineEvent(eventBody),
      {
        parse_mode: 'Markdown',
      },
    )
  }

  static sendPushMessage(chatId: number, eventBody: PushEventObject) {
    TGBot.getInstance().sendMessage(
      chatId,
      `➡️ Push at *${eventBody.ref}*\n` +
        `*By:* *${eventBody.user_username}* _(${eventBody.user_name})_\n` +
        `*Project:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Link to ${eventBody.object_kind} 🔗](${eventBody.commits[0].url})`,
      {
        parse_mode: 'Markdown',
      },
    )
  }
  static sendNoteMessage(chatId: number, eventBody: NoteEventObject) {
    TGBot.getInstance().sendMessage(
      chatId,
      `📝 Note: *${eventBody?.object_attributes?.description}*\n` +
        `*By:* *${eventBody.user.username}* _(${eventBody.user.name})_\n` +
        `*Project:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Link to ${eventBody?.object_kind} 🔗](${eventBody?.object_attributes?.url})`,
      {
        parse_mode: 'Markdown',
      },
    )
  }
}

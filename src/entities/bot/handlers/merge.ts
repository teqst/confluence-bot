import {MergeRequestActions, MergeRequestEventObject} from "../../../routes/gitHook/gitHook.types.js";

export function onMergeRequestEvent(eventBody: MergeRequestEventObject) {
    if (eventBody.object_attributes.action === MergeRequestActions.approved) {
        return approved(eventBody)
    }

    return merge(eventBody)
}

function approved(eventBody: MergeRequestEventObject): string {
    return  `✅ MR Approved by: *${eventBody.user.username}* _(${eventBody.user.name})_\n` +
        `_From_ *${eventBody.object_attributes.source_branch}* => _to_ *${eventBody.object_attributes.target_branch}*\n` +
        `*Project:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Link to ${eventBody.object_kind} 🔗](${eventBody.object_attributes.url})`
}

function merge(eventBody: MergeRequestEventObject): string {
    return  `📬 Merge request by: *${eventBody.user.username}* _(${eventBody.user.name})_\n` +
        `*Status:* ${eventBody.object_attributes.merge_status}\n` +
        `*State:* ${eventBody.object_attributes.state}\n` +
        `*Action:* ${eventBody.object_attributes.action}\n` +
        `_From_ *${eventBody.object_attributes.source_branch}* => _to_ *${eventBody.object_attributes.target_branch}*\n` +
        `*Project:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Link to ${eventBody.object_kind} 🔗](${eventBody.object_attributes.url})`
}

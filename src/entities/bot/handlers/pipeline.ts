import {PipelineEventObject, PipelineStatuses} from "../../../routes/gitHook/gitHook.types.js";

export function onPipelineEvent(eventBody: PipelineEventObject) {
    if (eventBody.object_attributes.detailed_status === PipelineStatuses.failed) {
        return failed(eventBody)
    }

    return ''
}

function failed(eventBody: PipelineEventObject): string {
    return  `🚀💥 Failed pipeline at *${eventBody.object_attributes.ref}*\n` +
        `*Status:* ${eventBody.object_attributes.status}\n` +
        `*Detailed status:* ${eventBody.object_attributes.detailed_status}\n` +
        `*Project:* [${eventBody.project.name}](${eventBody.project.web_url})\n` +
        `[Link to ${eventBody.object_kind} 🔗](${eventBody.project.web_url}/pipelines/${eventBody.object_attributes.id})`
}

import 'dotenv/config'
import express from 'express'
import TGBot from './entities/bot/bot.js'
import Githook from './routes/gitHook/gitHook.js'
import bodyParser from 'body-parser'
import {TelegramBotCommands} from './entities/bot/bot.types.js'
import {getChatIds, saveChatId} from './entities/chatsDb.js'
import {Message} from 'node-telegram-bot-api'
const port = process.env.PORT ?? 3000

const botInstance = TGBot.getInstance()

const app = express()
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
)

app.use(bodyParser.json())

app.listen(port, () => {
  botInstance.on('message', async (message: Message) => {
    const chatID = message.chat.id
    const data: Array<number> = []

    if (message.text === TelegramBotCommands.DAILY_JOKE) {
      await botInstance.sendMessage(chatID, `Version ${process.env.APP_VERSION}`)
    }

    if (message.text === TelegramBotCommands.VERSION) {
      await botInstance.sendMessage(chatID, `Version ${process.env.APP_VERSION}`)
    }

    if (message.text === TelegramBotCommands.START) {
      try {
        const allChatIds = await getChatIds()
        console.log(allChatIds)
        data.push(...allChatIds)
      } catch (err) {
        await saveChatId(chatID)
      }

      if (data.includes(chatID)) {
        await botInstance.sendMessage(chatID, `Привет, @${message.chat.username} ! Вы уже подписаны на оповещения.`)
      } else {
        await saveChatId(chatID)
        await botInstance.sendMessage(chatID, `Привет, @${message.chat.username} ! Вы добавлены в список оповещения.`)
      }
    }

    console.log('ChatIds:', data)
  })
})

app.use('/githook', Githook)

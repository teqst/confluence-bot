import express, { Request, Response } from 'express'
import {
  GitlabBaseHookReq,
  GitlabEventKind,
  MergeRequestEventObject,
  NoteEventObject,
  PipelineEventObject,
  PushEventObject,
} from './gitHook.types.js'
import TGBot from '../../entities/bot/bot.js'
import { getChatIds } from '../../entities/chatsDb.js'

const router = express.Router()

async function hookHandler(request: Request, response: Response) {
  const baseBody: GitlabBaseHookReq = request?.body
  const objectKind = baseBody?.object_kind

  if (!objectKind) response.end()

  const chatIds: Array<number> = await getChatIds()

  if (objectKind === GitlabEventKind.MERGE_REQUEST) {
    const eventBody = baseBody as MergeRequestEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId) {
        TGBot.sendMergeRequestMessage(chatId, eventBody)
      }
    })
  }

  if (objectKind === GitlabEventKind.PIPELINE) {
    const eventBody = baseBody as PipelineEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId) {
        TGBot.sendPipelineMessage(chatId, eventBody)
      }
    })
  }

  if (objectKind === GitlabEventKind.PUSH) {
    const eventBody = baseBody as PushEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId) {
        TGBot.sendPushMessage(chatId, eventBody)
      }
    })
  }

  if (objectKind === GitlabEventKind.NOTE) {
    const eventBody = baseBody as NoteEventObject

    chatIds.forEach((chatId: number) => {
      if (chatId) {
        TGBot.sendNoteMessage(chatId, eventBody)
      }
    })
  }

  response.end()
}

router.post('/', hookHandler)

export default router
